from schemas.tipoPublicacion import validatePostSchema, validatePutSchema
from flask import jsonify, request, Blueprint
import logging as log
from db.db import DB
from flask_jwt_extended import jwt_required

TipoPublicacion = Blueprint('TipoPublicacion', __name__)

logger = log.getLogger('errors_logger')

db = DB.getInstance()

@TipoPublicacion.route('/',methods=['GET'])
def get():
    try:
        lista = db.list('tipospublicaciones',None)
        return jsonify(lista)
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al leer los Tipos de Publicaciones.'}, 500

@TipoPublicacion.route('/<int:codigo>',methods=['GET'])
def getOne(codigo):
    try:
        registro = db.read('tipospublicaciones','codigo = {}'.format(codigo))
        if not(registro):
            return {'error': 'Registro no existe.'}, 400
        return jsonify(registro)
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al leer el Tipo de Publicación.'}, 500

@TipoPublicacion.route('/',methods=['POST'])
@jwt_required
def post():
    try:
        body = request.json
        if not(validatePostSchema(body)):
            return {'error': "Cuerpo del Tipo de Publicación invalido."}, 400
        codigo = body.get('codigo')
        if (db.read('tipospublicaciones','codigo = {}'.format(codigo))):
            return {'error': 'Registro ya existe.'}, 400
        db.create('tipospublicaciones',body)
        return {'msg': 'Registro creado.'}, 201
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al insertar el Tipo de Publicación.'}, 500

@TipoPublicacion.route('/<int:codigo>',methods=['PUT'])
@jwt_required
def put(codigo):
    try:
        body = request.json
        if not(validatePutSchema(body)):
            return {'error': "Cuerpo del Tipo de Publicación invalido."}, 400
        if not(db.read('tipospublicaciones','codigo = {}'.format(codigo))):
            return {'error': 'Registro no existe.'}, 400
        db.update('tipospublicaciones',body,'codigo = {}'.format(codigo))
        return {'msg': 'Registro actualizado.'}, 200
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al actualizar el Tipo de Publicación.'}, 500

@TipoPublicacion.route('/<int:codigo>',methods=['DELETE'])
@jwt_required
def delete(codigo):
    try:
        if not(db.read('tipospublicaciones','codigo = {}'.format(codigo))):
            return {'error': 'Registro no existe.'}, 400
        db.delete('tipospublicaciones','codigo = {}'.format(codigo))
        return {'msg': 'Registro borrado.'}, 200
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al borrar el Tipo de Publicación.'}, 500
