from schemas.usuario import validatePostSchema, validatePutSchema
from flask import jsonify, request, Blueprint
import logging as log
from db.db import DB
from hashlib import md5
from flask_jwt_extended import jwt_required

Usuario = Blueprint('Usuario', __name__)

logger = log.getLogger('errors_logger')

db = DB.getInstance()

@Usuario.route('/',methods=['GET'])
@jwt_required
def get():
    try:
        lista = db.list('usuarios',None)
        return jsonify(lista)
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al leer los Usuarios.'}, 500

@Usuario.route('/<string:identificacion>',methods=['GET'])
@jwt_required
def getOne(identificacion):
    try:
        registro = db.read('usuarios','identificacion = "{}"'.format(identificacion))
        if not(registro):
            return {'error': 'Registro no existe.'}, 400
        return jsonify(registro)
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al leer el Usuario.'}, 500

@Usuario.route('/',methods=['POST'])
@jwt_required
def post():
    try:
        body = request.json
        if not(validatePostSchema(body)):
            return {'error': "Cuerpo del Usuario invalido."}, 400
        identificacion = body.get('identificacion')
        if (db.read('usuarios','identificacion = "{}"'.format(identificacion))):
            return {'error': 'Registro ya existe.'}, 400
        body['clave'] = str(md5(body['clave'].encode()).hexdigest())
        db.create('usuarios',body)
        return {'msg': 'Registro creado.'}, 201
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al insertar el Usuario.'}, 500

@Usuario.route('/<string:identificacion>',methods=['PUT'])
@jwt_required
def put(identificacion):
    try:
        body = request.json
        if not(validatePutSchema(body)):
            return {'error': "Cuerpo del Usuario invalido."}, 400
        if not(db.read('usuarios','identificacion = "{}"'.format(identificacion))):
            return {'error': 'Registro no existe.'}, 400
        body['clave'] = str(md5(body['clave'].encode()).hexdigest())
        db.update('usuarios',body,'identificacion = "{}"'.format(identificacion))
        return {'msg': 'Registro actualizado.'}, 200
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al actualizar el Usuario.'}, 500

@Usuario.route('/<string:identificacion>',methods=['DELETE'])
@jwt_required
def delete(identificacion):
    try:
        if not(db.read('usuarios','identificacion = "{}"'.format(identificacion))):
            return {'error': 'Registro no existe.'}, 400
        db.delete('usuarios','identificacion = "{}"'.format(identificacion))
        return {'msg': 'Registro borrado.'}, 200
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al borrar el Usuario.'}, 500

def login(correo, clave):
    try:
        registro = db.read('usuarios','correo = "{}" AND clave = "{}"'.format(correo,clave))
        if not(registro):
            return False, 'Usuario/Clave invalido.'
        return True, registro
    except Exception as e:
        logger.exception(e)
        return False, e
