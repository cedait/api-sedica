from schemas.cadena import validatePostSchema, validatePutSchema
from flask import jsonify, request, Blueprint
import logging as log
from db.db import DB
from flask_jwt_extended import jwt_required

Cadena = Blueprint('Cadena', __name__)

logger = log.getLogger('errors_logger')

db = DB.getInstance()

@Cadena.route('/',methods=['GET'])
def get():
    try:
        lista = db.list('cadenas',None)
        return jsonify(lista)
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al leer las Cadenas.'}, 500

@Cadena.route('/<int:codigo>',methods=['GET'])
def getOne(codigo):
    try:
        registro = db.read('cadenas','codigo = {}'.format(codigo))
        if not(registro):
            return {'error': 'Registro no existe.'}, 400
        return jsonify(registro)
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al leer la Cadena.'}, 500

@Cadena.route('/',methods=['POST'])
@jwt_required
def post():
    try:
        body = request.json
        if not(validatePostSchema(body)):
            return {'error': "Cuerpo de la Cadena invalido."}, 400
        codigo = body.get('codigo')
        if (db.read('cadenas','codigo = {}'.format(codigo))):
            return {'error': 'Registro ya existe.'}, 400
        db.create('cadenas',body)
        return {'msg': 'Registro creado.'}, 201
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al insertar la Cadena.'}, 500

@Cadena.route('/<int:codigo>',methods=['PUT'])
@jwt_required
def put(codigo):
    try:
        body = request.json
        if not(validatePutSchema(body)):
            return {'error': "Cuerpo de la Cadena invalido."}, 400
        if not(db.read('cadenas','codigo = {}'.format(codigo))):
            return {'error': 'Registro no existe.'}, 400
        db.update('cadenas',body,'codigo = {}'.format(codigo))
        return {'msg': 'Registro actualizado.'}, 200
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al actualizar la Cadena.'}, 500

@Cadena.route('/<int:codigo>',methods=['DELETE'])
@jwt_required
def delete(codigo):
    try:
        if not(db.read('cadenas','codigo = {}'.format(codigo))):
            return {'error': 'Registro no existe.'}, 400
        db.delete('cadenas','codigo = {}'.format(codigo))
        return {'msg': 'Registro borrado.'}, 200
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al borrar la Cadena.'}, 500
