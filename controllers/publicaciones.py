from schemas.publicacion import validatePostSchema, validatePutSchema
from flask import jsonify, request, Blueprint
import logging as log
from db.db import DB
from flask_jwt_extended import jwt_required

Publicacion = Blueprint('Publicacion', __name__)

logger = log.getLogger('errors_logger')

db = DB.getInstance()

@Publicacion.route('/',methods=['GET'])
def get():
    try:
        lista = db.list('publicaciones',None)
        for i,p in enumerate(lista):
            lista[i]['etiquetas'] = formatLabels(p['etiquetas'])
        return jsonify(lista)
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al leer las Publicaciones.'}, 500

@Publicacion.route('/<int:codigo>',methods=['GET'])
def getOne(codigo):
    try:
        registro = db.read('publicaciones','codigo = {}'.format(codigo))
        if not(registro):
            return {'error': 'Registro no existe.'}, 400
        registro['etiquetas'] = formatLabels(registro['etiquetas'])
        return jsonify(registro)
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al leer la Publicación.'}, 500

@Publicacion.route('/cadena/<int:cadena>',methods=['GET'])
def getByCadena(cadena):
    try:
        lista = db.list('publicaciones','cadena = {}'.format(cadena))
        for i,p in enumerate(lista):
            lista[i]['etiquetas'] = formatLabels(p['etiquetas'])
        return jsonify(lista)
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al leer las Publicaciones.'}, 500

@Publicacion.route('/tipo/<int:tipo>',methods=['GET'])
def getByTipo(tipo):
    try:
        lista = db.list('publicaciones','tipo = {}'.format(tipo))
        for i,p in enumerate(lista):
            lista[i]['etiquetas'] = formatLabels(p['etiquetas'])
        return jsonify(lista)
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al leer las Publicaciones.'}, 500

@Publicacion.route('/etiqueta/<string:etiqueta>',methods=['GET'])
def getByEtiqueta(etiqueta):
    try:
        condicion = 'JSON_SEARCH(etiquetas, "one", "{}") IS NOT NULL'.format(etiqueta)
        lista = db.list('publicaciones',condicion)
        for i,p in enumerate(lista):
            lista[i]['etiquetas'] = formatLabels(p['etiquetas'])
        return jsonify(lista)
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al leer las Publicaciones.'}, 500

@Publicacion.route('/',methods=['POST'])
@jwt_required
def post():
    try:
        body = request.json
        if not(validatePostSchema(body)):
            return {'error': "Cuerpo de la Publicación invalido."}, 400
        if body['etiquetas'] is not None:
            body['etiquetas'] = str(body['etiquetas']).replace("'",'"')
        codigo = body.get('codigo')
        if (db.read('publicaciones','codigo = {}'.format(codigo))):
            return {'error': 'Registro ya existe.'}, 400
        db.create('publicaciones',body)
        return {'msg': 'Registro creado.'}, 201
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al insertar la Publicación.'}, 500

@Publicacion.route('/<int:codigo>',methods=['PUT'])
@jwt_required
def put(codigo):
    try:
        body = request.json
        if not(validatePutSchema(body)):
            return {'error': "Cuerpo de la Publicación invalido."}, 400
        if not(db.read('publicaciones','codigo = {}'.format(codigo))):
            return {'error': 'Registro no existe.'}, 400
        body['etiquetas'] = str(body['etiquetas']).replace("'",'"')
        db.update('publicaciones',body,'codigo = {}'.format(codigo))
        return {'msg': 'Registro actualizado.'}, 200
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al actualizar la Publicación.'}, 500

@Publicacion.route('/<int:codigo>',methods=['DELETE'])
@jwt_required
def delete(codigo):
    try:
        if not(db.read('publicaciones','codigo = {}'.format(codigo))):
            return {'error': 'Registro no existe.'}, 400
        db.delete('publicaciones','codigo = {}'.format(codigo))
        return {'msg': 'Registro borrado.'}, 200
    except Exception as e:
        logger.exception(e)
        return {'error': 'Error al borrar la Publicación.'}, 500

def formatLabels(etiquetas):
    if etiquetas is None:
        return None
    elif etiquetas == '[]':
        return []
    return (etiquetas.replace('"','')[1:-1].split(', '))
