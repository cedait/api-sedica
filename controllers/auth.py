from flask import request, jsonify, Blueprint
from flask_jwt_extended import create_access_token
import datetime as dt
from hashlib import md5

from schemas.auth import validate_login_schema
from controllers.usuarios import login as loginUser,post

Auth = Blueprint('Auth', __name__)

@Auth.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Falta body en el request"}), 400
    
    if not(validate_login_schema(request.json)):
        return jsonify({"msg": "Cuerpo invalido para login"}), 400

    correo = request.json.get('correo')
    clave = request.json.get('clave')    
    clave_md5 = str(md5(clave.encode()).hexdigest())

    user = loginUser(correo,clave_md5)

    if user[0] is False:
        return jsonify({"msg": str(user[1])}), 401
    access_token = create_access_token(identity=user[1], expires_delta=dt.timedelta(weeks=90))
    return jsonify(access_token=access_token), 200

@Auth.route('/singup', methods=['POST'])
def singup():
    return post
