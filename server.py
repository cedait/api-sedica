from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_jwt_extended import JWTManager
import logging as log
from dotenv import load_dotenv
import os

load_dotenv()

app = Flask(__name__)
CORS(app)

app.config['JSON_SORT_KEYS'] = False
app.config['JWT_SECRET_KEY'] = os.getenv('SEDICA_JWT_KEY')
jwt = JWTManager(app)

main_path = '/api/v1'

@app.route(main_path)
@app.route(main_path+'/')
def index():
    return jsonify({'api': 'SEDICA: Sistema Experto de Información y Comunicación Agropecuaria'}), 200

LOG_FORMAT = '%(levelname)s %(asctime)s - %(message)s'

logger = log.getLogger('errors_logger')
logger.setLevel(log.ERROR)
file_handler = log.FileHandler(os.getcwd()+'/logs/errors.log')
file_handler.setFormatter(log.Formatter(LOG_FORMAT))
logger.addHandler(file_handler)

'''ROUTES'''
#Import Controllers
from controllers.usuarios import Usuario
from controllers.cadenas import Cadena
from controllers.tiposPublicaciones import TipoPublicacion
from controllers.publicaciones import Publicacion
from controllers.auth import Auth
# Register routes
app.register_blueprint(Usuario,url_prefix=main_path+'/usuarios')
app.register_blueprint(Cadena,url_prefix=main_path+'/cadenas')
app.register_blueprint(TipoPublicacion,url_prefix=main_path+'/tiposPublicaciones')
app.register_blueprint(Publicacion,url_prefix=main_path+'/publicaciones')
app.register_blueprint(Auth,url_prefix=main_path+'/auth')
'''END ROUTES'''

'''ERRORS'''
@app.errorhandler(404)
def page_not_found(e):
    return jsonify({'error': 'Endpoint no hallado.'}), 404

@app.errorhandler(405)
def method_not_allow(e):
    return jsonify({'error': 'Método no permitido.'}),  405

@app.errorhandler(500)
def handle_500(e):
    logger.error(e)
    return jsonify({'error': 'Error del sistema en el servidor.'}), 500

@app.errorhandler(Exception)
def handle_exception(e):
    logger.error('EXCEPTION: '+str(e))
    return {'error': 'Ha ocurrido un error al ejecutar el servidor, si es necesario contacte al administrador.'}, 500 
'''END ERRORS'''

if __name__ == "__main__":
    app.run(port=5000,debug=True)