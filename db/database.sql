-- **********************************
-- *********** Database *************
-- **********************************
create database if not exists sedica;
use sedica;
-- **********************************
-- ************ Tables **************
-- **********************************

-- table publicaciones
CREATE TABLE  publicaciones (
	usuario VARCHAR (30) NOT NULL 	,
	cadena INTEGER (6) NOT NULL 	,
	tipo INTEGER (6) NOT NULL 	,
	codigo INTEGER (6) NOT NULL 	,
	titulo VARCHAR (200) NOT NULL 	,
	nombre VARCHAR (200) NOT NULL 	,
	fecha DATETIME NOT NULL 	,
	tematica VARCHAR (500) NOT NULL 	,
	resumen VARCHAR (500) NOT NULL 	,
	imagen VARCHAR (500) NULL 	,
	archivo VARCHAR (500) NULL 	,
	etiquetas JSON NULL 	,
	CONSTRAINT  pk_Publicacion 
	PRIMARY KEY( codigo )
);


-- table tipospublicaciones
CREATE TABLE  tipospublicaciones (
	codigo INTEGER (6) NOT NULL 	,
	nombre VARCHAR (200) NOT NULL 	,
	descripcion VARCHAR (500) NULL 	,
	CONSTRAINT  pk_TipoPublicacion 
	PRIMARY KEY( codigo )
);


-- table usuarios
CREATE TABLE  usuarios (
	identificacion VARCHAR (30) NOT NULL 	,
	nombre VARCHAR (200) NOT NULL 	,
	correo VARCHAR (200) NOT NULL 	,
	clave VARCHAR (50) NOT NULL 	,
	estado VARCHAR (50) NOT NULL 	,
	CONSTRAINT  pk_Usuario 
	PRIMARY KEY( identificacion )
);


-- table cadenas
CREATE TABLE  cadenas (
	codigo INTEGER (6) NOT NULL 	,
	nombre VARCHAR (200) NOT NULL 	,
	descripcion VARCHAR (500) NULL 	,
	CONSTRAINT  pk_Cadena 
	PRIMARY KEY( codigo )
);

-- ****************************************
-- ************ Foreign Keys **************
-- ****************************************

-- For publicaciones(fk_Publicacion_Usuario) 
ALTER TABLE publicaciones ADD(
	CONSTRAINT fk_Publicacion_Usuario
	FOREIGN KEY ( usuario )
	REFERENCES  usuarios ( identificacion )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);
-- For publicaciones(fk_Publicacion_Cadena) 
ALTER TABLE publicaciones ADD(
	CONSTRAINT fk_Publicacion_Cadena
	FOREIGN KEY ( cadena )
	REFERENCES  cadenas ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);
-- For publicaciones(fk_Publicacion_TipoPublicacion) 
ALTER TABLE publicaciones ADD(
	CONSTRAINT fk_Publicacion_TipoPublicacion
	FOREIGN KEY ( tipo )
	REFERENCES  tipospublicaciones ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE
);
