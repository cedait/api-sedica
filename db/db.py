import mysql.connector as mysql
from dotenv import load_dotenv
import os
from functools import wraps

load_dotenv()

_host = os.getenv('SEDICA_HOST')
_port = os.getenv('SEDICA_PORT')
_user = os.getenv('SEDICA_USER')
_password = os.getenv('SEDICA_PASSWORD')
_database = os.getenv('SEDICA_DATABASE')


def validate_connection(f):
    @wraps(f)
    def decorated_function(self,*args, **kwargs):
        if not(self.cnx):
            self.connect()
            raise Exception('Conexion caida!')
        else:
            try:
                return f(self, *args, **kwargs)
            except:
                self.close_connection()
                self.connect()
                raise Exception('Conexion caida!')
    return decorated_function


class DB:
    __instance = None
    
    @staticmethod
    def getInstance():
        if DB.__instance == None:
            DB()
        return DB.__instance
    
    def __init__(self):
        if DB.__instance != None:
            raise Exception("Esta clase es un Singleton!")
        else:
            self.cnx = None
            self.connect()          
            DB.__instance = self
    
    def connect(self):
        try:
            self.cnx = mysql.MySQLConnection(
                host=_host,
                port=_port,
                user=_user,
                password=_password,
                database=_database,
            )
        except:
            self.close_connection()
            
    def close_connection(self):
        if self.cnx:
            if self.cnx.is_connected():
                connection.close()
        self.cnx = None

    @validate_connection
    def create(self,tabla,body):
        columns = str(tuple(body.keys())).replace("'", "`")
        values = str(tuple(body.values()))
        sql = 'INSERT INTO {} {} VALUES{}'.format(tabla, columns, values).replace('None','null')
        cur = self.cnx.cursor()
        cur.execute(sql)
        self.cnx.commit()
        cur.close
        self.cnx.close

    @validate_connection
    def read(self,tabla,clave):
        cur = self.cnx.cursor()
        cur.execute('SELECT * FROM {} WHERE {}'.format(tabla,clave))
        row = cur.fetchone()
        if not(row) : return False
        columns = [i[0] for i in cur.description]
        registro = zip(columns, row)
        cur.close
        self.cnx.close
        return (dict(registro))

    @validate_connection
    def update(self,tabla,body,clave):
        setValues =  "`"+str(body)[2:-1].replace("':", "`=").replace(", '",", `")
        sql = 'UPDATE {} SET {} WHERE {}'.format(tabla,setValues,clave).replace('None','NULL')
        cur = self.cnx.cursor()
        cur.execute(sql)
        self.cnx.commit()
        cur.close
        self.cnx.close

    @validate_connection
    def delete(self,tabla,clave):
        cur = self.cnx.cursor()
        cur.execute('DELETE FROM {} WHERE {}'.format(tabla,clave))
        self.cnx.commit()
        cur.close
        self.cnx.close
    
    @validate_connection
    def list(self,tabla,condicion):
        cur = self.cnx.cursor()
        if not condicion: cur.execute('SELECT * FROM {}'.format(tabla))
        else: cur.execute('SELECT * FROM {} WHERE {}'.format(tabla,condicion))
        lista = []
        rows = cur.fetchall()
        columns = [i[0] for i in cur.description]
        for row in rows:
            registro = zip(columns, row)
            json = dict(registro)
            lista.append(json)
        cur.close
        self.cnx.close
        return (lista)