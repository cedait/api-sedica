from jsonschema import Draft7Validator, draft7_format_checker

post_schema = {
    "type": "object",
    "properties": {
        "identificacion":  {"type": "string", "maxLength": 30},
        "nombre":  {"type": "string", "maxLength": 200},
        "correo":  {"type": "string", "format": "email", "maxLength": 200},
        "clave":  {"type": "string", "maxLength": 50},
        "estado":  {"type": "string", "maxLength": 50},
    },
    "required": [ "identificacion", "nombre", "correo", "clave", "estado" ],
    "additionalProperties": False
}

put_schema = {
    "type": "object",
    "minProperties": 1,
    "properties": {
        "nombre":  {"type": "string", "maxLength": 200},
        "correo":  {"type": "string", "format": "email", "maxLength": 200},
        "clave":  {"type": "string", "maxLength": 50},
        "estado":  {"type": "string", "maxLength": 50},
    },
    "additionalProperties": False
}

def validatePostSchema(json):
    return Draft7Validator(post_schema,format_checker=draft7_format_checker).is_valid(json)

def validatePutSchema(json):
    return Draft7Validator(put_schema,format_checker=draft7_format_checker).is_valid(json)
