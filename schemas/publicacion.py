from jsonschema import Draft7Validator, draft7_format_checker

post_schema = {
    "type": "object",
    "properties": {
        "usuario":  {"type": "string", "maxLength": 30},
        "cadena":  {"type": "integer", "maxLength": 6},
        "tipo":  {"type": "integer", "maxLength": 6},
        "codigo":  {"type": "integer", "maxLength": 6},
        "titulo":  {"type": "string", "maxLength": 200},
        "nombre":  {"type": "string", "maxLength": 200},
        "fecha":  {"type": "string", "format": "date"},
        "tematica":  {"type": "string", "maxLength": 500},
        "resumen":  {"type": "string", "maxLength": 500},
        "imagen":  {"type": ["string", "null"], "maxLength": 500},
        "archivo":  {"type": ["string", "null"], "maxLength": 500},
        "etiquetas":  {"type": ["array", "null"], "items": {"type": "string"}},
    },
    "required": [ "usuario", "cadena", "tipo", "codigo", "titulo", "nombre", "fecha", "tematica", "resumen" ],
    "additionalProperties": False
}

put_schema = {
    "type": "object",
    "minProperties": 1,
    "properties": {
        "cadena":  {"type": "integer", "maxLength": 6},
        "tipo":  {"type": "integer", "maxLength": 6},
        "titulo":  {"type": "string", "maxLength": 200},
        "nombre":  {"type": "string", "maxLength": 200},
        "fecha":  {"type": "string", "format": "date"},
        "tematica":  {"type": "string", "maxLength": 500},
        "resumen":  {"type": "string", "maxLength": 500},
        "imagen":  {"type": ["string", "null"], "maxLength": 500},
        "archivo":  {"type": ["string", "null"], "maxLength": 500},
        "etiquetas":  {"type": ["array", "null"], "items": {"type": "string"}},
    },
    "additionalProperties": False
}

def validatePostSchema(json):
    return Draft7Validator(post_schema,format_checker=draft7_format_checker).is_valid(json)

def validatePutSchema(json):
    return Draft7Validator(put_schema,format_checker=draft7_format_checker).is_valid(json)
