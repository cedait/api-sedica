from jsonschema import Draft7Validator, draft7_format_checker

post_schema = {
    "type": "object",
    "properties": {
        "codigo":  {"type": "integer", "maxLength": 6},
        "nombre":  {"type": "string", "maxLength": 200},
        "descripcion":  {"type": ["string", "null"], "maxLength": 500},
    },
    "required": [ "codigo", "nombre" ],
    "additionalProperties": False
}

put_schema = {
    "type": "object",
    "minProperties": 1,
    "properties": {
        "nombre":  {"type": "string", "maxLength": 200},
        "descripcion":  {"type": "string", "maxLength": 500},
    },
    "additionalProperties": False
}

def validatePostSchema(json):
    return Draft7Validator(post_schema,format_checker=draft7_format_checker).is_valid(json)

def validatePutSchema(json):
    return Draft7Validator(put_schema,format_checker=draft7_format_checker).is_valid(json)
